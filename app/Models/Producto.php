<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    use HasFactory;

    protected $appends = ['id_encrypt'];

    public function getIdEncryptAttribute()
    {
        return encrypt($this->id);
    }
}
