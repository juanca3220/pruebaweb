<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ProductoController extends Controller
{

    public function index()
    {
        return Inertia::render('Productos/Index');
    }

    public function listaJs()
    {

        $query = Producto::select('id', 'nombre', 'precio', 'marca');

        return datatables()->of($query)
            ->addColumn('idEncrypt', function($query) {
                return encrypt($query->id);
            })
            ->toJson();

    }

    public function create()
    {
        return Inertia::render('Productos/Create');
    }

    public function store(Request $request)
    {

        $request->validate([
            'nombre' => 'required|max:30',
            'precio' => 'required|numeric',
            'marca' => 'required',
            'descripcion' => 'required'
        ]);

        $producto = new Producto();
        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;
        $producto->marca = $request->marca;
        $producto->descripcion = $request->descripcion;
        $producto->save();

        return redirect('productos');
    }

    public function show($idEncrypt)
    {
        $id = decrypt($idEncrypt);

        $producto = Producto::find($id);

        return Inertia::render('Productos/Show', compact('producto'));
    }

    public function edit($idEncrypt)
    {
        $id = decrypt($idEncrypt);

        $producto = Producto::find($id);

        return Inertia::render('Productos/Edit', compact('producto'));
    }

    public function update(Request $request, Producto $producto)
    {
        $request->validate([
            'nombre' => 'required|max:30',
            'precio' => 'required|numeric',
            'marca' => 'required',
            'descripcion' => 'required'
        ]);

        $producto->nombre = $request->nombre;
        $producto->precio = $request->precio;
        $producto->marca = $request->marca;
        $producto->descripcion = $request->descripcion;
        $producto->save();

        return redirect('productos');
    }

    public function destroy(Producto $producto)
    {
        if($producto){
            $producto->delete();
        }

        return redirect('productos');
    }
}
